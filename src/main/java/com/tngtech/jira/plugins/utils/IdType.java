package com.tngtech.jira.plugins.utils;

public enum IdType {
	Project, Filter, Undefined
}
